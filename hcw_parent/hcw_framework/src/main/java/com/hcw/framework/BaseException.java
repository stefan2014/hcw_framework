package com.hcw.framework;

/**
 * 系统异常. 作为华夏框架异常基类以便扩展对系统异常的统一处理逻辑，如：异常日志等.
 * 
 * @author TrueOrFalse.Yuan
 */
public class BaseException extends Exception {

    private static final long serialVersionUID = 2168543136020027550L;

    protected String exceptionCode;

    /**
     * 构造函数.
     * 
     * @param exceptionCode
     *            异常号
     */
    public BaseException(String exceptionCode) {
        this(exceptionCode, "");
    }

    /**
     * 构造函数.
     * 
     * @param exceptionCode
     *            异常号
     * @param exceptionMessage
     *            异常消息
     */
    public BaseException(String exceptionCode, String exceptionMessage) {
        super(exceptionMessage);
        this.exceptionCode = exceptionCode;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }

    public void setExceptionCode(String exceptionCode) {
        this.exceptionCode = exceptionCode;
    }

}
