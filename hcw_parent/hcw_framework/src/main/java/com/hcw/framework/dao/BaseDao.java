package com.hcw.framework.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

/**
 *
 * @ClassName： BaseDao @Description： 基础DAO操作
 * 
 * @author Huang,ChunWu
 * @date 2015年9月24日 下午5:00:53
 *
 */
public interface BaseDao<E> {

	/**
	 * @param entity
	 * @return
	 */
	public Integer add(E entity) throws DataAccessException;

	/**
	 * @param entity
	 * @return
	 */
	public void update(E entity) throws DataAccessException;

	/**
	 * 删除一个实体
	 * @param entity
	 */
	public void delete(E entity) throws DataAccessException;

	public E getEntityById(Integer pri_key) throws DataAccessException;
	
	public List<E> queryList(Integer pri_key)throws DataAccessException;
	
	public Integer getQueryCount()throws DataAccessException;

}
