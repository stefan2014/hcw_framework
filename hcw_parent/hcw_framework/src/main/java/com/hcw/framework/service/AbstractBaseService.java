package com.hcw.framework.service;

import org.springframework.dao.DataAccessException;

import com.hcw.framework.dao.BaseDao;
import com.hcw.framework.web.bean.DefaultMessageCode;

/**
 *
 * @ClassName： AbstractBaseService @Description： 在此输入功能描述
 * 
 * @author Huang,ChunWu
 * @date 2015年9月24日 下午4:51:46
 *
 */
public abstract class AbstractBaseService<E> implements BaseService<E> {

	protected abstract BaseDao getBaseDao();

	@Override
	public Integer add(E entity) throws ServiceException {
		try {
			return getBaseDao().add(entity);
		} catch (DataAccessException e) {
			throw new ServiceException(DefaultMessageCode.WM_GLOBAL_METHOD_ERROR_PARAM, e.getLocalizedMessage());
		}
	}

	@Override
	public void update(E entity) throws ServiceException {
		try {
			getBaseDao().update(entity);
		} catch (DataAccessException e) {
			throw new ServiceException("数据库更新异常", e.getLocalizedMessage());
		}
	}

	@Override
	public void delete(E entity) throws ServiceException {
		try {
			getBaseDao().delete(entity);
		} catch (DataAccessException e) {
			throw new ServiceException("数据删除异常", e.getLocalizedMessage());
		}
	}

}
