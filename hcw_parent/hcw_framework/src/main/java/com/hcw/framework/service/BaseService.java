package com.hcw.framework.service;

/**
 *
 * @ClassName： BaseService 
 * @Description： 基础service层
 * @author Huang,ChunWu
 * @date 2015年9月24日 下午4:05:33
 *
 */
public interface BaseService<E> {

	/**
	 * 通用对象保存方法
	 * @param entity
	 */
	public Integer add(E entity) throws ServiceException;
	
	public void update(E entity) throws ServiceException;
	
	
	public void delete(E entity) throws ServiceException;

}
