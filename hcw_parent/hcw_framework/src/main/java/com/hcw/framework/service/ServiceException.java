package com.hcw.framework.service;

import com.hcw.framework.BaseException;

/**
*
* @ClassName： ServiceException
* @Description： 在此输入功能描述
* @author Huang,ChunWu
* @date 2015年9月24日 下午5:30:12
*
*/
public class ServiceException extends BaseException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5376979801837576575L;
	
	
	/**
	 * @param exceptionCode
	 * @param exceptionMessage
	 */
	public ServiceException(String exceptionCode, String exceptionMessage) {
		super(exceptionCode, exceptionMessage);
	}
	
	/**
	 * @param exceptionCode
	 */
	public ServiceException(String exceptionCode) {
		super(exceptionCode);
	}


}
