package com.hcw.framework.utils;

import java.util.Locale;

import com.hcw.framework.web.bean.OperateResult;
import com.hcw.framework.web.bean.OperateResult.ResultType;

public class OperateResultUtils {

	public static OperateResult generateSUCCESS(String messageCode, Locale locale) {
		OperateResult result = new OperateResult();
		result.setType(ResultType.SUCCESS);
		result.setMessage(WebMessageUtils.generateINFO(messageCode, locale));
		return result;
	}

	public static OperateResult generateFAILURE(String messageCode, Locale locale) {
		OperateResult result = new OperateResult();
		result.setType(ResultType.FAILURE);
		result.setMessage(WebMessageUtils.generateERROR(messageCode, locale));
		return result;
	}

	public static OperateResult generateVERIFY(String messageCode, Locale locale) {
		OperateResult result = new OperateResult();
		result.setType(ResultType.VERIFY);
		result.setMessage(WebMessageUtils.generateWARN(messageCode, locale));
		return result;
	}
}
