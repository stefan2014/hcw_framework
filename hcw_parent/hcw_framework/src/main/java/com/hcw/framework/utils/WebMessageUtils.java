package com.hcw.framework.utils;

import java.util.Locale;

import org.springframework.context.MessageSource;

import com.hcw.framework.web.bean.WebMessage;

public class WebMessageUtils {

	protected static MessageSource messageSource;

	public MessageSource getMessageSource() {
		return messageSource;
	}

	public void setMessageSource(MessageSource messageSource) {
		WebMessageUtils.messageSource = messageSource;
	}

	public static WebMessage generateINFO(String MessageCode, Locale locale) {
		return new WebMessage(WebMessage.WM_TYPE_INFO, getMessage(MessageCode, locale));
	}

	public static WebMessage generateERROR(String MessageCode, Locale locale) {
		return new WebMessage(WebMessage.WM_TYPE_INFO, getMessage(MessageCode, locale));
	}

	public static WebMessage generateWARN(String MessageCode, Locale locale) {
		return new WebMessage(WebMessage.WM_TYPE_INFO, getMessage(MessageCode, locale));
	}

	public static String getMessage(String messageCode, Locale locale) {
		return messageSource.getMessage(messageCode, null, locale);
	}
}
