package com.hcw.framework.web.bean;

/**
 * 默认Web消息号常量定义.
 * 
 * @author TrueOrFalse.Yuan
 */
public class DefaultMessageCode {

    public static final String WM_GLOBAL_METHOD_SUCCESS = "global.method.success";

    public static final String WM_GLOBAL_METHOD_ERROR_SERVICE = "global.method.error.service";

    public static final String WM_GLOBAL_METHOD_ERROR_PARAM = "global.method.error.param";

    public static final String WM_GLOBAL_METHOD_ERROR_PERMISSION = "global.method.error.permission";

    public static final String WM_GLOBAL_METHOD_ERROR_NOTEXISTED = "global.method.error.notexisted";

    public static final String WM_GLOBAL_CREATE_SUCCESS = "global.create.success";

    public static final String WM_GLOBAL_CREATE_EXISTED = "global.create.existed";
    
    public static final String WM_GLOBAL_CREATE_FAILED = "global.create.failed";

    public static final String WM_GLOBAL_UPDATE_SUCCESS = "global.update.success";

    public static final String WM_GLOBAL_UPDATE_EXISTED = "global.update.existed";

    public static final String WM_GLOBAL_UPDATE_NOTEXISTED = "global.update.notexisted";
    
    public static final String WM_GLOBAL_UPDATE_FAIL = "global.update.fail";

    public static final String WM_GLOBAL_DELETE_SUCCESS = "global.delete.success";
    
    public static final String WM_GLOBAL_DELETE_FAILED = "global.delete.failed";

    public static final String WM_GLOBAL_DELETE_NOTEXISTED = "global.delete.notexisted";

    public static final String WM_GLOBAL_BATCHDELETE_SUCCESS = "global.batchdelete.success";

    public static final String WM_GLOBAL_BATCHDELETE_FAILED = "global.batchdelete.failed";

    public static final String WM_GLOBAL_BATCHDELETE_ALLFAILED = "global.batchdelete.allfailed";

}
