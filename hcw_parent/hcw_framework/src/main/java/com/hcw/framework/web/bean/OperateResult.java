package com.hcw.framework.web.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * Web操作结果
 * @author huangcw
 */
public class OperateResult {

	/**
	 * 成功，失败，需验证
	 */
	public enum ResultType {
		SUCCESS, FAILURE, VERIFY
	}

	private ResultType type;

	private WebMessage message;

	private Map<String, String> values;

	public ResultType getType() {
		return type;
	}

	public void setType(ResultType type) {
		this.type = type;
	}

	public WebMessage getMessage() {
		return message;
	}

	public void setMessage(WebMessage message) {
		this.message = message;
	}

	public Map<String, String> getValues() {
		return values;
	}

	public void setValues(Map<String, String> values) {
		this.values = values;
	}

	public void addValue(String key, String value) {
		if (values == null) {
			values = new HashMap<String, String>();
		}
		values.put(key, value);
	}
}
