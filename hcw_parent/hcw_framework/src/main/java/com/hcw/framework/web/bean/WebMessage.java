package com.hcw.framework.web.bean;

/**
 * Web消息, 用于Ajax返回系统提示或异常信息.
 * @author huangcw
 */
public class WebMessage {

	public static final String WM_TYPE_INFO = "INFO";

	public static final String WM_TYPE_WARN = "WARN";

	public static final String WM_TYPE_ERROR = "ERROR";

	/**
	 * 消息类型.
	 */
	private String messageType;

	/**
	 * 消息内容.
	 */
	private String messageContent;

	public WebMessage(String messageType, String messageContent) {
		super();
		this.messageType = messageType;
		this.messageContent = messageContent;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

}
