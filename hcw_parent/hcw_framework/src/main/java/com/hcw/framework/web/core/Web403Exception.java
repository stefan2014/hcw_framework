package com.hcw.framework.web.core;

import com.hcw.framework.BaseException;

/**
 * @author TrueOrFalse.Yuan
 */
public class Web403Exception extends BaseException {

	private static final long serialVersionUID = 8200302022426846900L;

	public Web403Exception(String exceptionCode) {
		super(exceptionCode);
	}
}
