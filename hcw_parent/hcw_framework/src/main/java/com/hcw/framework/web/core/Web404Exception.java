package com.hcw.framework.web.core;

import com.hcw.framework.BaseException;

/**
 * Web非法异常的包装类. 包括非正常的参数输入,错误id等，作为404错误返回
 * 
 * @author TrueOrFalse.Yuan
 */
public class Web404Exception extends BaseException {

	private static final long serialVersionUID = -1062122655387822574L;

	public Web404Exception(String exceptionCode) {
		super(exceptionCode);
	}
}
