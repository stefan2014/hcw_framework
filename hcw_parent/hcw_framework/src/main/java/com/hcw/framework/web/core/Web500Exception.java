package com.hcw.framework.web.core;

import com.hcw.framework.BaseException;

/**
 * @author TrueOrFalse.Yuan
 */
public class Web500Exception extends BaseException {

	private static final long serialVersionUID = 4042608279336187559L;

	public Web500Exception(String exceptionCode) {
		super(exceptionCode);
	}
}
