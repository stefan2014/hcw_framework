package com.hcw.framework.web.core;

import com.hcw.framework.BaseException;

/**
 * Web异常.
 * 
 * @author TrueOrFalse.Yuan
 */
public class WebException extends BaseException {

	private static final long serialVersionUID = 3814195047938048790L;

	/**
	 * 构造函数.
	 * 
	 * @param exceptionCode
	 *            异常号
	 * @param exceptionMessage
	 *            异常消息
	 */
	public WebException(String exceptionCode, String exceptionMessage) {
		super(exceptionCode, exceptionMessage);
	}

}
