package com.hcw.framework.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hcw.framework.entity.Account;
import com.hcw.framework.service.AccountService;
import com.hcw.framework.service.BaseService;
import com.hcw.framework.web.core.BaseController;

/**
 * 
 * @author yang
 * @version 2015-04-15 13:44:42
 * @see com.yang.spinach.account.web.Account
 */
@Controller
@RequestMapping(value = "/user")
public class AccountController extends BaseController<Account> {
	@Resource
	private AccountService accountService;

	/* (non-Javadoc)
	 * @see com.hcw.framework.web.core.BaseController#getBaseService()
	 */
	@Override
	protected BaseService<Account> getBaseService() {
		return accountService;
	}

}
