package com.hcw.framework.dao;

import org.springframework.stereotype.Repository;

/**
 * 
 * @author <Auto generate>
 * @version 2015-04-15 13:44:42
 * @see com.yang.spinach.account.dao.Account
 */
@Repository
public interface AccountDao extends BaseDao {

}
