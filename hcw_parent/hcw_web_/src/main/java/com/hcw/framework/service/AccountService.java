package com.hcw.framework.service;

import com.hcw.framework.entity.Account;

/**
 * 
 * @author <Auto generate>
 * @version 2015-04-15 13:44:42
 * @see com.yang.spinach.account.service.Account
 */
public interface AccountService extends BaseService<Account> {
	

}
