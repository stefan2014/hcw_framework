package com.hcw.framework.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.hcw.framework.dao.AccountDao;
import com.hcw.framework.dao.BaseDao;
import com.hcw.framework.entity.Account;

@Service("accountService")
public class AccountServiceImpl extends AbstractBaseService<Account> implements AccountService{
	@Resource
	private AccountDao accountDao;

	/* (non-Javadoc)
	 * @see com.hcw.framework.service.AbstractBaseService#getBaseDao()
	 */
	@Override
	protected BaseDao getBaseDao() {
		return accountDao;
	}
	

}
